import { FC, useMemo } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  Button as MaterialButton,
  IconButton,
  CircularProgress,
  Tooltip,
} from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  button: {
    minWidth: '6rem',
    margin: '1rem',
  },
  circularProgress: {
    color: theme.palette.primary.contrastText,
  },
}));

export interface ButtonProps {
  label: string;
  icon?: JSX.Element | null;
  onClick?: any;
  disabled?: boolean;
  loading?: boolean;
  submit?: boolean;
  color?: 'primary' | 'secondary';
}

const Button: FC<ButtonProps> = ({
  label,
  onClick,
  icon = null,
  disabled = false,
  loading = false,
  submit = false,
  color = 'primary',
}) => {
  const classes = useStyles();

  const iconBtn = useMemo(() => {
    return (
      <Tooltip title={label} placement="top-end">
        <IconButton
          key={`icon-button-${label}`}
          data-testid={`icon-button-${label}`}
          onClick={onClick}
          disabled={disabled}
          color={color}
          size="medium"
          type={submit ? 'submit' : 'button'}
        >
          {icon}
        </IconButton>
      </Tooltip>
    );
  }, [disabled, label, icon, color, onClick, submit]);

  const containedBtn = useMemo(() => {
    return (
      <MaterialButton
        key={`button-${label}`}
        data-testid={`button-${label}`}
        onClick={onClick}
        disabled={disabled || loading}
        className={classes.button}
        color={color}
        type={submit ? 'submit' : 'button'}
        variant="contained"
      >
        {loading ? (
          <CircularProgress size={24} className={classes.circularProgress} />
        ) : (
          label
        )}
      </MaterialButton>
    );
  }, [classes, disabled, label, loading, color, onClick, submit]);

  return <>{icon ? iconBtn : containedBtn}</>;
};

export default Button;

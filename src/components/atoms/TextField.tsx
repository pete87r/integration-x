import { FC, FocusEventHandler, ChangeEventHandler } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import MaterialTextField from '@material-ui/core/TextField';

const useStyles = makeStyles((theme) => ({
  text: {
    color: theme.palette.text.primary,
    fontWeight: 400,
    fontSize: '0.9rem',
  },
}));

export interface TextFieldProps {
  value?: string;
  name: string;
  id?: string | number;
  onBlur?: FocusEventHandler<HTMLInputElement>;
  onFocus?: FocusEventHandler<HTMLInputElement>;
  onChange?: ChangeEventHandler<HTMLInputElement>;
  disabled?: boolean;
  error?: any;
  multiline?: boolean;
  date?: boolean;
  helperText?: any;
}

const TextField: FC<TextFieldProps> = ({
  value,
  name,
  id,
  onBlur,
  onFocus,
  onChange,
  disabled = false,
  error,
  multiline = false,
  date = false,
  helperText = '',
}) => {
  const classes = useStyles();

  return (
    <MaterialTextField
      data-testid={id ? `text-field-${id}` : 'text-field'}
      disabled={disabled}
      key={`text-field-${id}`}
      onBlur={onBlur}
      onFocus={onFocus}
      onChange={onChange}
      value={value}
      name={name}
      helperText={helperText}
      error={error}
      fullWidth
      margin="dense"
      variant="outlined"
      multiline={multiline}
      rows={multiline ? 5 : 1}
      InputProps={{ className: classes.text }}
      type={date ? 'date' : 'text'}
    />
  );
};

export default TextField;

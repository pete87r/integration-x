import { FC, MouseEventHandler } from 'react';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  '@keyframes openAnimation': {
    from: {
      opacity: 0,
    },
    to: {
      opacity: 1,
    },
  },
  '@keyframes closeAnimation': {
    from: {
      opacity: 1,
    },
    to: {
      opacity: 0,
    },
  },
  backdropContainerBlur: {
    position: 'fixed',
    zIndex: 1,
    opacity: 0,
    width: '100vw',
    height: '100vh',
    background: 'rgba(0,0,0,0.3)',
    animation: '$openAnimation 1000ms ease-out forwards',
  },
  closeBackdropAnimation: {
    opacity: 1,
    animation: '$closeAnimation 1000ms ease-out forwards',
  },
});

export interface BackdropProps {
  onClick: MouseEventHandler<HTMLDivElement>;
  isClosing: boolean;
}

const Backdrop: FC<BackdropProps> = ({ onClick, isClosing }) => {
  const classes = useStyles();

  return (
    <div
      data-testid="backdrop"
      className={`${classes.backdropContainerBlur} ${
        isClosing && classes.closeBackdropAnimation
      }`}
      onClick={onClick}
    />
  );
};

export default Backdrop;

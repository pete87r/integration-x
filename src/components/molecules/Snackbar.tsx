import { FC } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  Snackbar as MaterialSnackbar,
  SnackbarContent,
  Typography,
} from '@material-ui/core';
import { Button } from '../atoms';
import CloseIcon from '@material-ui/icons/Close';

const useStyles = makeStyles((theme) => ({
  success: {
    background: theme.palette.success.main,
  },
  info: {
    background: theme.palette.info.main,
  },
  error: {
    background: theme.palette.error.main,
  },
  message: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
}));

export enum AlertTypes {
  SUCCESS = 'success',
  INFO = 'info',
  ERROR = 'error',
}

export interface SnackbarProps {
  type: AlertTypes;
  handleClose: any;
  open: boolean;
  onExited: any;
  message: string;
}

const Snackbar: FC<SnackbarProps> = ({
  type,
  handleClose,
  open,
  onExited,
  message,
}) => {
  const classes = useStyles();

  return (
    <MaterialSnackbar
      anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
      open={open}
      onClose={handleClose}
      message={message}
      onExited={onExited}
      autoHideDuration={5000}
    >
      <SnackbarContent
        className={`${classes[type]}`}
        message={
          <div className={classes.message}>
            <Typography variant="body1" color="secondary">
              {message}
            </Typography>
            <Button
              color="secondary"
              icon={<CloseIcon />}
              label="Close Window"
              onClick={handleClose}
            />
          </div>
        }
      />
    </MaterialSnackbar>
  );
};

export default Snackbar;

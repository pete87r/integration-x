import { FC } from 'react';
import ReactDOM from 'react-dom';
import { makeStyles } from '@material-ui/core/styles';
import { useDispatch, useSelector } from 'react-redux';
import {
  closeSnackbar,
  removeSnackbar,
  snackbars as snackbarsSelector,
} from '../../store/alertsSlice';
import Snackbar from './Snackbar';

const useStyles = makeStyles({
  alerts: {
    zIndex: 10000,
    position: 'fixed',
    top: '1rem',
    left: '50%',
    transform: 'translateX(-50%)',
    width: '80vw',
  },
});

const Alert: FC = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const snackbars = useSelector(snackbarsSelector);

  const onSnackbarClose = (id: string) => {
    dispatch(closeSnackbar(id));
  };

  const onSnackbarExited = (id: string) => {
    dispatch(removeSnackbar(id));
  };

  return (
    <>
      {ReactDOM.createPortal(
        <div className={classes.alerts} data-testid="alert">
          {snackbars?.map((snackbar) => (
            <Snackbar
              key={snackbar.id}
              message={snackbar.message}
              type={snackbar.type}
              open={!snackbar.closed}
              handleClose={() => onSnackbarClose(snackbar.id)}
              onExited={() => onSnackbarExited(snackbar.id)}
            />
          ))}
        </div>,
        document.getElementById('modal-root')!
      )}
    </>
  );
};

export default Alert;

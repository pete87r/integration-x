import { FC, useMemo, useCallback } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import Skeleton from '@material-ui/lab/Skeleton';
import {
  Card,
  CardContent,
  Table as MaterialTable,
  TableHead,
  TableRow,
  TableCell,
  Typography,
  TableBody,
} from '@material-ui/core';
import { Button } from '../atoms';

const useStyles = makeStyles((theme: any) => ({
  head: {
    background: theme.palette.primary.main,
    height: '4rem',
  },
  body: {
    position: 'fixed',
    zIndex: 1,
    width: '100%',
    height: '100%',
    opacity: 0,
  },
  cell: {
    maxWidth: '150px',
  },
  activeCell: {
    width: '180px',
  },
  text: {
    overflow: 'hidden',
    textOverflow: 'ellipsis',
  },
  empty: {
    height: '5rem',
  },
  skeleton: {
    height: '4rem',
  },
}));

export interface Column {
  key: string;
  name: string;
}

export interface Content {
  key: string;
  value: any;
}

export interface TableProps {
  action: boolean;
  columns: Array<Column>;
  content: Array<Content>;
  id: string | number;
  onOpenClick: Function;
  onEditClick: Function;
  onDeleteClick: Function;
  helperText?: string;
  isFetching?: boolean;
}

const Table: FC<TableProps> = ({
  action,
  columns,
  content,
  id,
  onOpenClick,
  onEditClick,
  onDeleteClick,
  helperText,
  isFetching,
}) => {
  const classes = useStyles();

  const getSpanRow = useCallback(
    (child: JSX.Element) => {
      return (
        <TableRow className={classes.empty}>
          <TableCell colSpan={columns.length + 1} align="center">
            {child}
          </TableCell>
        </TableRow>
      );
    },
    [columns.length, classes]
  );

  const contentSection = useMemo(() => {
    return content.map((row) => (
      <TableRow key={row.key}>
        {columns.map((column) => (
          <TableCell key={`${row.key}-${column.key}`} className={classes.cell}>
            <Typography variant="body1" className={classes.text}>
              {row.value[column.key]}
            </Typography>
          </TableCell>
        ))}
        {action && (
          <TableCell
            key={`action-section-${row.key}`}
            data-testid={`action-section-${row.key}`}
            className={classes.activeCell}
          >
            <Button
              icon={<ArrowForwardIosIcon />}
              label="Open"
              onClick={() => onOpenClick(row)}
            />
            <Button
              icon={<EditIcon />}
              label="Edit"
              onClick={() => onEditClick(row)}
            />
            <Button
              icon={<DeleteIcon />}
              label="Delete"
              onClick={() => onDeleteClick(row)}
            />
          </TableCell>
        )}
      </TableRow>
    ));
  }, [
    content,
    columns,
    action,
    onDeleteClick,
    onEditClick,
    onOpenClick,
    classes,
  ]);

  return (
    <Card>
      <CardContent>
        <MaterialTable data-testid={id}>
          <TableHead>
            <TableRow className={classes.head} key="header">
              {columns.map((column) => (
                <TableCell key={column.key}>
                  <Typography variant="h3">{column.name}</Typography>
                </TableCell>
              ))}
              {action && (
                <TableCell key="action-header">
                  <Typography variant="h3">Action</Typography>
                </TableCell>
              )}
            </TableRow>
          </TableHead>
          <TableBody>
            {isFetching
              ? getSpanRow(
                  <Skeleton variant="rect" className={classes.skeleton} />
                )
              : helperText
              ? getSpanRow(<Typography variant="h3">{helperText}</Typography>)
              : contentSection}
          </TableBody>
        </MaterialTable>
      </CardContent>
    </Card>
  );
};

export default Table;

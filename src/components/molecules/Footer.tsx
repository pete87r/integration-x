import { FC } from 'react';
import { Typography, BottomNavigation } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  footer: {
    zIndex: 1,
    height: '64px',
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    background: '#00a3c6',
    boxShadow:
      '0px -2px 4px 0px rgba(0,0,0,0.2),0px -4px 5px 0px rgba(0,0,0,0.14),0px -1px 10px 0px rgba(0,0,0,0.12)',
  },
});

const Footer: FC = () => {
  const classes = useStyles();

  return (
    <BottomNavigation className={classes.footer}>
      <Typography variant="body2">
        © Copyright - Piotr Fleszar and Myszka - The Programming Cat, 2021 -
        Poland
      </Typography>
    </BottomNavigation>
  );
};
export default Footer;

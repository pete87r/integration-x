import { FC } from 'react';
import { AppBar, Toolbar, makeStyles, Typography } from '@material-ui/core';
import { Link } from 'react-router-dom';

const useStyles = makeStyles({
  appBar: {
    background: '#00a3c6',
    height: '64px',
  },
  toolbar: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  menuSection: {
    display: 'flex',
    alignItems: 'center',
  },
});

const NavBar: FC = () => {
  const classes = useStyles();

  return (
    <AppBar position="static" className={classes.appBar}>
      <Toolbar className={classes.toolbar}>
        <Link to="/articles">
          <Typography variant="h1">Integration-X DEMO</Typography>
        </Link>
      </Toolbar>
    </AppBar>
  );
};

export default NavBar;

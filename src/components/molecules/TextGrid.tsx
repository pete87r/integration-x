import { FC } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextCell, { TextCellProps } from './TextCell';

const useStyles = makeStyles({
  gridContainer: {
    display: 'grid',
    padding: '1rem',
    gridTemplateColumns: 'repeat(auto-fit, minmax(250px, 1fr))',
    gridGap: '1rem',
  },
});

export interface TextGridProps {
  content: Array<TextCellProps>;
}

const TextGrid: FC<TextGridProps> = ({ content }) => {
  const classes = useStyles();

  return (
    <div className={classes.gridContainer} key="text-grid">
      {content.map((item) => (
        <TextCell
          title={item.title}
          description={item.description}
          keyValue={item.keyValue}
          key={item.keyValue}
        />
      ))}
    </div>
  );
};
export default TextGrid;

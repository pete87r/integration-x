import { FC, useMemo } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Typography } from '@material-ui/core';
import { Formik, Form as FormikForm, FormikProps } from 'formik';
import * as Yup from 'yup';
import { Button, TextField } from '../atoms';

const useStyles = makeStyles({
  gridContainer: {
    display: 'grid',
    padding: '1rem',
    gridGap: '1rem',
    gridTemplateColumns: 'repeat(auto-fit, minmax(250px, 1fr))',
  },
  cell: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    width: '100%',
  },
  actionSection: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  span: {
    gridColumn: '1',
    // eslint-disable-next-line no-useless-computed-key
    ['@media (min-width:550px)']: {
      gridColumn: 'span 2',
    },
    // eslint-disable-next-line no-useless-computed-key
    ['@media (min-width:830px)']: {
      gridColumn: 'span 3',
    },
  },
});

interface ValidationSchema {
  minLength?: number;
  maxLength?: number;
}

interface Value {
  name: string;
  label: string;
  multiline?: boolean;
  date?: boolean;
  span?: boolean;
  validationSchema?: ValidationSchema;
}

export interface FormProps {
  content: Array<Value>;
  initialValues: any;
  handleSubmit: Function;
}

const Form: FC<FormProps> = ({ content, initialValues, handleSubmit }) => {
  const classes = useStyles();

  const validationMap = useMemo(() => {
    const map: any = {};
    content.forEach((field) => {
      const validationSchema = field.validationSchema;
      if (validationSchema) {
        if (field.date) {
          map[field.name] = Yup.date().required().label(field.label);
        } else {
          map[field.name] = Yup.string()
            .required()
            .min(validationSchema.minLength || 0)
            .max(validationSchema.maxLength || 256)
            .label(field.label);
        }
      }
    });
    return map;
  }, [content]);

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={Yup.object().shape(validationMap)}
      onSubmit={(values: object) => {
        handleSubmit(JSON.stringify(values));
      }}
    >
      {(props: FormikProps<any>) => {
        return (
          <FormikForm>
            <div className={classes.gridContainer} key="form">
              {content.map((field) => (
                <div
                  className={`${classes.cell} ${field.span && classes.span}`}
                  key={`container-${field.name}`}
                >
                  <Typography variant="h3" key={`label-${field.name}`}>
                    {field.label}
                  </Typography>
                  <TextField
                    id={field.name}
                    name={field.name}
                    onBlur={props.handleBlur}
                    onChange={props.handleChange}
                    value={props.values[field.name]}
                    multiline={field.multiline}
                    date={field.date}
                    error={
                      props.touched[field.name] && props.errors[field.name]
                    }
                    helperText={
                      props.touched[field.name] && props.errors[field.name]
                    }
                  />
                </div>
              ))}
            </div>
            <div className={classes.actionSection} key="form-actions">
              <Button submit label="save" />
              <Button label="reset" onClick={props.resetForm} />
            </div>
          </FormikForm>
        );
      }}
    </Formik>
  );
};
export default Form;

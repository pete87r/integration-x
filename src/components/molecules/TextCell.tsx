import { FC } from 'react';
import { Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  textCellContainer: {
    display: 'flex',
    alignItems: 'baseline',
    justifyContent: 'flex-start',
    width: '100%',
    padding: '0.5rem',
  },
  title: {
    paddingRight: '1rem',
  },
  description: {
    paddingRight: '1rem',
    overflowWrap: 'anywhere',
  },
});

export interface TextCellProps {
  title: string;
  description: any;
  keyValue: string;
}

const TextCell: FC<TextCellProps> = ({ title, description, keyValue }) => {
  const classes = useStyles();

  return (
    <div className={classes.textCellContainer} key={`container-${keyValue}`}>
      <Typography
        variant="h3"
        className={classes.title}
        key={`title-${keyValue}`}
      >
        {title}
      </Typography>
      <Typography
        variant="body1"
        className={classes.description}
        key={`description-${keyValue}`}
      >
        {description}
      </Typography>
    </div>
  );
};

export default TextCell;

import { FC } from 'react';
import { useHistory } from 'react-router-dom';
import { Table } from '../molecules';
import { Content } from '../molecules/Table';

const articlesTableColumns = [
  {
    key: 'title',
    name: 'Title',
  },
  {
    key: 'author',
    name: 'Author',
  },
  {
    key: 'subject',
    name: 'Subject',
  },
  {
    key: 'createDate',
    name: 'Create Date',
  },
  {
    key: 'releaseDate',
    name: 'Release Date',
  },
];

export interface ArticlesTableProps {
  articles: any;
  isFetching: boolean;
  helperText?: string;
  onEditClick: Function;
  onDeleteClick: Function;
}

const ArticlesTable: FC<ArticlesTableProps> = ({
  articles,
  isFetching,
  helperText,
  onEditClick,
  onDeleteClick,
}) => {
  const history = useHistory();

  const handleOpenClick = (element: Content) => {
    history.push(`articles/${element.key}`);
  };

  return (
    <div>
      <Table
        action={true}
        columns={articlesTableColumns}
        content={articles}
        id="articles-table"
        helperText={helperText}
        onOpenClick={handleOpenClick}
        onEditClick={onEditClick}
        onDeleteClick={onDeleteClick}
        isFetching={isFetching}
      />
    </div>
  );
};
export default ArticlesTable;

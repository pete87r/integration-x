import { Card, Typography } from '@material-ui/core';
import { FC } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import ReactDOM from 'react-dom';
import { Backdrop, Button } from '../atoms';
import { Form } from '../molecules';
import { ArticleProps } from '../pages/Article';
import CloseIcon from '@material-ui/icons/Close';
import { createOrUpdateArticles } from '../../api/articlesApi';
import { useDispatch } from 'react-redux';
import { addSnackbar } from '../../store/alertsSlice';
import { AlertTypes } from '../molecules/Snackbar';

const useStyles = makeStyles({
  '@keyframes openAnimation': {
    '0%': {
      top: '-100vh',
    },
    '90%': {
      top: '82px',
    },
    '100%': {
      top: '64px',
    },
  },
  '@keyframes closeAnimation': {
    '0%': {
      top: '64px',
    },
    '10%': {
      top: '82px',
    },
    '100%': {
      top: '-100vh',
    },
  },
  '@keyframes openAnimationMobile': {
    '0%': {
      top: '-100vh',
    },
    '100%': {
      top: '-1vh',
    },
  },
  '@keyframes closeAnimationMobile': {
    '0%': {
      top: '-1vh',
    },
    '100%': {
      top: '-100vh',
    },
  },
  root: {
    zIndex: 1200,
    position: 'fixed',
    maxWidth: '1000px',
    width: '100vw',
    height: '102vh',
    top: '-100vh',
    left: 0,
    background: '#fff',
    padding: '1rem',
    animation: '$openAnimationMobile 1000ms ease-out forwards',
    overflowY: 'auto',
    // eslint-disable-next-line no-useless-computed-key
    ['@media (min-width:860px)']: {
      animation: '$openAnimation 1000ms ease-out forwards',
      zIndex: 1,
      maxHeight: 'calc(100% - 128px)',
      height: 'auto',
      margin: '1rem 0 ',
      left: '50%',
      transform: 'translate(-50%)',
    },
  },
  closeModalAnimation: {
    animation: '$closeAnimationMobile 1000ms ease-out forwards',
    // eslint-disable-next-line no-useless-computed-key
    ['@media (min-width:860px)']: {
      animation: '$closeAnimation 1000ms ease-out forwards',
    },
  },
  headerSection: {
    display: 'flex',
    alignItems: 'flex-start',
    justifyContent: 'space-between',
    width: '100%',
    padding: '0.5rem',
  },
});

const articleFormContent = [
  {
    name: 'title',
    label: 'Title',
    validationSchema: {
      minLength: 2,
      maxLength: 100,
    },
  },
  {
    name: 'author',
    label: 'Author',
    validationSchema: {
      minLength: 2,
      maxLength: 100,
    },
  },
  {
    name: 'subject',
    label: 'Subject',
    validationSchema: {
      minLength: 2,
      maxLength: 256,
    },
  },
  {
    name: 'createDate',
    label: 'Create Date',
    date: true,
    validationSchema: {},
  },
  {
    name: 'releaseDate',
    label: 'Release Date',
    date: true,
    validationSchema: {},
  },
  {
    name: 'text',
    label: 'Text',
    multiline: true,
    span: true,
    validationSchema: {
      minLength: 2,
      maxLength: 2000,
    },
  },
];

export interface ArticleCreatorProps {
  closeModal: any;
  reload: Function;
  article: ArticleProps;
  isClosing: boolean;
  id?: string;
}

const ArticleCreator: FC<ArticleCreatorProps> = ({
  closeModal,
  article,
  id,
  isClosing,
  reload,
}) => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const handleSubmit = (article: any) => {
    createOrUpdateArticles(article, id)
      .then((result) => {
        dispatch(
          addSnackbar({
            id: new Date().getTime(),
            type: AlertTypes.SUCCESS,
            message: `Article successfully ${id ? 'updated' : 'created'}!`,
          })
        );
        closeModal();
        reload();
      })
      .catch(() => {
        dispatch(
          addSnackbar({
            id: new Date().getTime(),
            type: AlertTypes.ERROR,
            message: `Ups! Something went wrong, unable to ${
              id ? 'update' : 'create'
            } article`,
          })
        );
        closeModal();
        reload();
      });
  };

  return (
    <>
      {ReactDOM.createPortal(
        <Backdrop onClick={closeModal} isClosing={isClosing} />,
        document.getElementById('backdrop-root')!
      )}
      {ReactDOM.createPortal(
        <Card
          className={`${classes.root} ${
            isClosing && classes.closeModalAnimation
          }`}
        >
          <div className={classes.headerSection}>
            <Typography variant="h2">
              {id ? 'Edit Article' : 'Create new Article'}
            </Typography>
            <Button
              icon={<CloseIcon />}
              label="Close Window"
              onClick={closeModal}
            />
          </div>
          <Form
            content={articleFormContent}
            initialValues={article}
            handleSubmit={handleSubmit}
          />
        </Card>,
        document.getElementById('modal-root')!
      )}
    </>
  );
};

export default ArticleCreator;

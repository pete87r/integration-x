import { FC } from 'react';
import { useHistory } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import { Divider, Typography } from '@material-ui/core';
import { TextGrid } from '../molecules';
import { Button } from '../atoms';
import { ArticleProps } from '../pages/Article';
import Skeleton from '@material-ui/lab/Skeleton';

const useStyles = makeStyles((theme) => ({
  mainContainer: {
    display: 'grid',
    gridTemplateColumns: '1fr minmax(350px, 1000px) 1fr',
    margin: '5rem 0',
    height: 'fit-content',
  },
  contentContainer: {
    border: `solid 3px ${theme.palette.primary.main}`,
    margin: '1rem',
  },
  titleSection: {
    background: theme.palette.primary.main,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    padding: '1rem',
    margin: '1rem 1rem 0 1rem',
  },
  title: {
    overflowWrap: 'anywhere',
  },
  detailsSection: {
    padding: '1rem',
  },
  textSection: {
    padding: '3rem 2.5rem',
  },
  divider: {
    background: theme.palette.primary.main,
    height: '3px',
    margin: '0 1rem',
  },
  skeleton: {
    height: '5rem',
  },
  helperText: {
    textAlign: 'center',
    margin: '2rem',
  },
  buttonContainer: {
    display: 'flex',
    justifyContent: 'center',
  },
}));

export interface ArticleDetailsProps {
  article?: ArticleProps;
  isFetching: boolean;
}

const ArticleDetails: FC<ArticleDetailsProps> = ({ article, isFetching }) => {
  const classes = useStyles();
  const history = useHistory();

  const parseDetails = () => {
    return [
      {
        title: 'Title',
        description: article!.title,
        keyValue: 'title',
      },
      {
        title: 'Author',
        description: article!.author,
        keyValue: 'author',
      },
      {
        title: 'Subject',
        description: article!.subject,
        keyValue: 'subject',
      },
      {
        title: 'Create Date',
        description: article!.createDate,
        keyValue: 'createDate',
      },
      {
        title: 'Release Date',
        description: article!.releaseDate,
        keyValue: 'releaseDate',
      },
    ];
  };

  const handleBackClick = () => {
    history.push('/articles');
  };

  return (
    <div className={classes.mainContainer} key="article-details-container">
      <div key="empty-container-1" />
      <div>
        <div
          className={classes.contentContainer}
          key="article-content-container"
        >
          {isFetching && (
            <Skeleton
              variant="rect"
              className={classes.skeleton}
              key="article-skeleton"
            />
          )}
          {article && (
            <>
              <div
                className={classes.titleSection}
                key="article-title-container"
              >
                <Typography
                  variant="h2"
                  className={classes.title}
                  key="article-title"
                >
                  {article.title}
                </Typography>
              </div>
              <div className={classes.detailsSection} key="article-grid">
                <TextGrid content={parseDetails()} />
              </div>
              <Divider
                variant="fullWidth"
                className={classes.divider}
                key="article-divider"
              />
              <div className={classes.textSection} key="article-text-container">
                <Typography variant="body1" key="article-text">
                  {article.text}
                </Typography>
              </div>
            </>
          )}
        </div>
        <div className={classes.buttonContainer} key="article-button-container">
          <Button label="Back" onClick={handleBackClick} />
        </div>
      </div>
      <div key="empty-container-2" />
    </div>
  );
};

export default ArticleDetails;

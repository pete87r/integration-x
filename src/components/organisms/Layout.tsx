import { FC } from 'react';
import Footer from '../molecules/Footer';
import NavBar from '../molecules/NavBar';
const Layout: FC = (props) => {
  return (
    <>
      <NavBar />
      {props.children}
      <Footer />
    </>
  );
};

export default Layout;

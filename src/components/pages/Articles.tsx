import { FC, useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { ArticlesTable } from '../organisms';
import { Button } from '../atoms';
import { Typography } from '@material-ui/core';
import { getArticles, deleteArticle } from '../../api/articlesApi';
import AddIcon from '@material-ui/icons/Add';
import ArticleCreator from '../organisms/ArticleCreator';
import { Content } from '../molecules/Table';
import { ArticleProps } from './Article';
import { useDispatch } from 'react-redux';
import { addSnackbar } from '../../store/alertsSlice';
import { AlertTypes } from '../molecules/Snackbar';
import { CancelToken } from '../../api/axios';

const useStyles = makeStyles((theme) => ({
  mainContainer: {
    display: 'grid',
    gridTemplateColumns: '1fr minmax(750px, 1000px) 1fr',
    margin: '5rem 0',
    paddingBottom: '2rem',
  },
  headerSection: {
    display: 'flex',
    alignItems: 'flex-start',
    justifyContent: 'space-between',
    width: '100%',
    padding: '0.5rem',
  },
}));

const articleInitialValues = {
  title: '',
  author: '',
  subject: '',
  createDate: '',
  releaseDate: '',
  text: '',
};

const Articles: FC = () => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const [openModal, setOpenModal] = useState<boolean>(false);
  const [isClosing, setIsClosing] = useState<boolean>(false);
  const [articles, setArticles] = useState<any>([]);
  const [isFetching, setIsFetching] = useState<boolean>(false);
  const [helperText, setHelperText] = useState<string | undefined>(undefined);
  const [articleId, setArticleId] = useState<string | undefined>(undefined);
  const [initialValues, setInitialValues] =
    useState<ArticleProps>(articleInitialValues);
  const [reload, setReload] = useState<boolean>(true);

  useEffect(() => {
    if (reload) {
      const cancelToken = CancelToken.source().token;
      setIsFetching(true);
      setReload(false);
      getArticles(cancelToken)
        .then((result: object) => {
          const fetchedArticles: Array<Content> = [];
          result &&
            Object.entries(result).forEach((article) => {
              fetchedArticles.push({ key: article[0], value: article[1] });
            });
          setArticles(fetchedArticles);
          setHelperText(result ? undefined : 'Sorry, there is nothing to show');
          setIsFetching(false);
        })
        .catch((error) => {
          if (error?.message !== 'canceled') {
            dispatch(
              addSnackbar({
                id: new Date().getTime(),
                type: AlertTypes.ERROR,
                message: 'Ups! Something went wrong, unable to fetch data',
              })
            );
            setHelperText('Unable to fetch data!');
            setIsFetching(false);
          }
        });
    }
  }, [reload, dispatch]);

  const handleEditClick = (element: Content) => {
    setInitialValues(element.value);
    setArticleId(element.key);
    setOpenModal(true);
  };

  const handleDeleteClick = (element: Content) => {
    deleteArticle(element.key)
      .then(() => {
        setReload(true);
        dispatch(
          addSnackbar({
            id: new Date().getTime(),
            type: AlertTypes.SUCCESS,
            message: `Article ${element.value.title} has been removed successfully`,
          })
        );
      })
      .catch(() => {
        dispatch(
          addSnackbar({
            id: new Date().getTime(),
            type: AlertTypes.ERROR,
            message: 'Ups! Something went wrong, unable to remove article',
          })
        );
      });
  };

  const handleClose = () => {
    setTimeout(() => {
      setOpenModal(false);
      setIsClosing(false);
    }, 1000);
    setIsClosing(true);
    setArticleId(undefined);
    setInitialValues(articleInitialValues);
  };

  return (
    <div className={classes.mainContainer}>
      <div />
      <div>
        <div className={classes.headerSection}>
          <Typography variant="h2">Articles</Typography>
          <Button
            icon={<AddIcon />}
            label="Add New Article"
            onClick={() => setOpenModal(true)}
          />
        </div>
        <ArticlesTable
          articles={articles}
          isFetching={isFetching}
          helperText={helperText}
          onEditClick={handleEditClick}
          onDeleteClick={handleDeleteClick}
        />
        {openModal && (
          <ArticleCreator
            article={initialValues}
            id={articleId}
            closeModal={handleClose}
            isClosing={isClosing}
            reload={() => setReload(true)}
          />
        )}
      </div>
      <div />
    </div>
  );
};

export default Articles;

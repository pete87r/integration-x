import { FC, useEffect, useState } from 'react';
import ArticleDetails from '../organisms/ArticleDetails';
import { getArticle } from '../../api/articlesApi';
import { useParams } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { addSnackbar } from '../../store/alertsSlice';
import { AlertTypes } from '../molecules/Snackbar';
import { CancelToken } from '../../api/axios';
import { useHistory } from 'react-router-dom';

export interface ArticleProps {
  title: string;
  author: string;
  subject: string;
  createDate: string;
  releaseDate: string;
  text: string;
}

interface ParamTypes {
  articleId: string;
}

const Article: FC = () => {
  const { articleId } = useParams<ParamTypes>();
  const dispatch = useDispatch();
  const history = useHistory();

  const [article, setArticle] = useState<ArticleProps | undefined>(undefined);
  const [isFetching, setIsFetching] = useState<boolean>(false);

  useEffect(() => {
    const cancelToken = CancelToken.source().token;
    setIsFetching(true);
    getArticle(articleId, cancelToken)
      .then((result: any) => {
        if (!result) {
          throw Error;
        }
        setArticle(result);
        setIsFetching(false);
      })
      .catch((error) => {
        if (error?.message !== 'canceled') {
          dispatch(
            addSnackbar({
              id: new Date().getTime(),
              type: AlertTypes.ERROR,
              message: `Ups! Something went wrong, unable to fetch data`,
            })
          );
          setIsFetching(false);
          history.replace('/articles');
        }
      });
  }, [articleId, dispatch, history]);

  return <ArticleDetails article={article} isFetching={isFetching} />;
};

export default Article;

import axiosInstance from './axios';

export const getArticles = (cancelToken?: any) => {
  return axiosInstance.get('/articles.json', { cancelToken });
};

export const getArticle = (id: string, cancelToken?: any) => {
  return axiosInstance.get(`/articles/${id}.json`, { cancelToken });
};

export const createOrUpdateArticles = (body: object, id?: string) => {
  return id
    ? axiosInstance.put(`/articles/${id}.json`, body)
    : axiosInstance.post('/articles.json', body);
};

export const deleteArticle = (id: string) => {
  return axiosInstance.delete(`/articles/${id}.json`);
};

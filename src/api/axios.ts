import axios from 'axios';

export const CancelToken = axios.CancelToken;

const axiosInstance = axios.create({
  baseURL: 'https://integration-x-demo-default-rtdb.firebaseio.com/',
  headers: {
    Accept: 'application/json',
  },
});

axiosInstance.interceptors.request.use(
  (request) => {
    request.headers['Content-Type'] = 'application/json';
    return request;
  },
  (error) => {
    return Promise.reject(error);
  }
);

axiosInstance.interceptors.response.use(
  (response) => {
    return response.data;
  },
  (error) => {
    return Promise.reject(error);
  }
);

export default axiosInstance;

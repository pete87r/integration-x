import { Table } from '../../components/molecules';
import { fireEvent, render } from '@testing-library/react';
import { tableColumns, tableContent } from '../../utils/mockedData';

const fieldsToCheck = [
  'Name',
  'Country',
  'City',
  'Address',
  'Zip Code',
  'Action',
  'John',
  'England',
  'London',
  'Oxford Street',
  '12-345',
  'Jacob',
  'Denmark',
  'Copenhagen',
  'Amager',
  '98-521',
  'Piotr',
  'Poland',
  'Zielona Gora',
  'Wojska Polskiego',
  '45-123',
];

describe('Component:Molecules:Table', () => {
  test('should render table and handle onClick', () => {
    const mockFn = jest.fn();

    const { getByText, getByTestId, getAllByTestId } = render(
      <Table
        action={true}
        columns={tableColumns}
        content={tableContent}
        id="test-table"
        onOpenClick={mockFn}
        onDeleteClick={mockFn}
        onEditClick={mockFn}
      />
    );
    getByTestId('test-table');
    fieldsToCheck.forEach((field) => getByText(field));
    fireEvent.click(getAllByTestId('icon-button-Open')[0]);
    fireEvent.click(getAllByTestId('icon-button-Edit')[0]);
    fireEvent.click(getAllByTestId('icon-button-Delete')[0]);
    expect(mockFn).toHaveBeenCalledTimes(3);
  });
});

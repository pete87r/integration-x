import { Backdrop } from '../../components/atoms';
import { fireEvent, render } from '@testing-library/react';

describe('Component:Atom:Backdrop', () => {
  test('should render backdrop', () => {
    const mockFn = jest.fn();

    const { getByTestId } = render(
      <Backdrop onClick={mockFn} isClosing={false} />
    );
    expect(getByTestId('backdrop'));
    fireEvent.click(getByTestId('backdrop'));
    expect(mockFn).toHaveBeenCalledTimes(1);
  });
});

import Button from '../../components/atoms/Button';
import { fireEvent, render } from '@testing-library/react';

describe('Component:Atom:Button', () => {
  test('should render button and handle onClick', () => {
    const mockFn = jest.fn();

    const { getByText, getByTestId } = render(
      <Button onClick={mockFn} label="Test" />
    );
    getByTestId('button-Test');
    getByText('Test');
    fireEvent.click(getByTestId('button-Test'));
    expect(mockFn).toHaveBeenCalledTimes(1);
  });

  test('should render icon button', () => {
    const mockFn = jest.fn();

    const { getByTestId } = render(
      <Button onClick={mockFn} label="Test" icon={<p />} />
    );
    getByTestId('icon-button-Test');
  });

  test('should render disabled button and ignore onClick', () => {
    const mockFn = jest.fn();

    const { getByText, getByTestId } = render(
      <Button onClick={mockFn} label="Test" disabled />
    );
    expect(getByTestId('button-Test')).toHaveAttribute('disabled');
    getByText('Test');
    fireEvent.click(getByTestId('button-Test'));
    expect(mockFn).toHaveBeenCalledTimes(0);
  });
});

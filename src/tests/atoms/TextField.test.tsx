import { TextField } from '../../components/atoms';
import { render } from '@testing-library/react';

describe('Component:Atom:TextField', () => {
  test('should render text field', () => {
    const { getByTestId, getByRole, getByDisplayValue } = render(
      <TextField value="Test" name="test" />
    );
    getByTestId('text-field');
    getByRole('textbox');
    getByDisplayValue('Test');
  });
});

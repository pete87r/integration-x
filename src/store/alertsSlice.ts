import { createSlice } from '@reduxjs/toolkit';
import { RootState } from './store';

interface SliceState {
  snackbars: Array<any>;
}

const initialState: SliceState = {
  snackbars: [],
};

const alertSlice = createSlice({
  name: 'alerts',
  initialState,
  reducers: {
    addSnackbar: (state, { payload }) => {
      state.snackbars.push(payload);
    },
    closeSnackbar: (state, { payload }) => {
      const index = state.snackbars.findIndex(
        (snackbar) => snackbar.id === payload
      );
      state.snackbars[index].closed = true;
    },
    removeSnackbar: (state, { payload }) => {
      state.snackbars = state.snackbars.filter(
        (snackbar) => snackbar.id !== payload
      );
    },
  },
});

export const { addSnackbar, closeSnackbar, removeSnackbar } =
  alertSlice.actions;

export const snackbars = (state: RootState) => state.alerts.snackbars;

export default alertSlice.reducer;

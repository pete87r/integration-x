import { configureStore } from '@reduxjs/toolkit';
import alerts from './alertsSlice';

const store = configureStore({
  reducer: {
    alerts,
  },
});

export type RootState = ReturnType<typeof store.getState>;

export default store;

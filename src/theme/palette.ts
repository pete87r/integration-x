const palette = {
  primary: {
    main: '#00a3c6',
    light: '#00d2ff',
    contrastText: '#fff',
  },
  secondary: {
    main: '#e6e6e6',
    dark: '#292929',
    contrastText: '#292929',
  },
  text: {
    primary: '#292929',
    secondary: '#00a3c6',
  },
};

export default palette;

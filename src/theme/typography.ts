import palette from './palette';

const typography = {
  fontFamily: 'Montserrat',
  h1: {
    color: palette.text.primary,
    fontWeight: 600,
    fontSize: '1.8rem',
    letterSpacing: '0.2rem',
  },
  h2: {
    color: palette.text.primary,
    fontWeight: 600,
    fontSize: '1.5rem',
    letterSpacing: '0.2rem',
  },
  h3: {
    color: palette.text.primary,
    fontWeight: 600,
    fontSize: '1rem',
    letterSpacing: '0.1rem',
  },
  body1: {
    color: palette.text.primary,
    fontWeight: 400,
    fontSize: '0.9rem',
  },
  body2: {
    color: palette.text.primary,
    fontWeight: 400,
    fontSize: '0.8rem',
  },
};

export default typography;

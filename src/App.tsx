import { FC, lazy, Suspense } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { CircularProgress } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const Articles = lazy(() => import('./components/pages/Articles'));
const Article = lazy(() => import('./components/pages/Article'));

const useStyles = makeStyles({
  spinnerContainer: {
    height: '100',
  },
  spinner: {
    position: 'fixed',
    top: '50%',
    left: '50%',
    transform: 'translate(-50% -50%)',
  },
});

const App: FC = () => {
  const classes = useStyles();

  return (
    <Suspense
      fallback={
        <div className={classes.spinnerContainer}>
          <CircularProgress className={classes.spinner} />
        </div>
      }
    >
      <Switch>
        <Route path="/articles" exact>
          <Articles />
        </Route>
        <Route path="/articles/:articleId">
          <Article />
        </Route>
        <Redirect to="/articles" />
      </Switch>
    </Suspense>
  );
};

export default App;

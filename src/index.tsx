import ReactDOM from 'react-dom';
import { ThemeProvider } from '@material-ui/core/styles';
import { BrowserRouter } from 'react-router-dom';
import theme from '../src/theme/index';
import './index.css';
import App from './App';
import { Provider } from 'react-redux';
import store from './store/store';
import { Alert } from './components/molecules';
import Layout from './components/organisms/Layout';

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <BrowserRouter>
      <Provider store={store}>
        <Alert />
        <Layout>
          <App />
        </Layout>
      </Provider>
    </BrowserRouter>
  </ThemeProvider>,
  document.getElementById('root')
);

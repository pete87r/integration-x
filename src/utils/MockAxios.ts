import { useEffect } from 'react';
import MockAdapter from 'axios-mock-adapter';
import api from '../api/axios';
import { article } from './mockedData';

const apiMock = new MockAdapter(api);

const apiCalls = [
  {
    req: '/articles.json',
    res: [{ key: 1, value: article }],
  },
  {
    req: '/articles/1.json',
    res: article,
  },
];

const MockAxios = ({ children }) => {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  useEffect(() => {
    apiCalls.forEach((call) => {
      apiMock.onGet(call.req).reply(200, call.res);
    });
    return () => {
      apiMock.reset();
    };
  });
  return children;
};

export default MockAxios;

export const tableColumns = [
  {
    key: 'name',
    name: 'Name',
  },
  {
    key: 'country',
    name: 'Country',
  },
  {
    key: 'city',
    name: 'City',
  },
  {
    key: 'address',
    name: 'Address',
  },
  {
    key: 'zipCode',
    name: 'Zip Code',
  },
];

export const tableContent = [
  {
    key: 'john-row',
    value: {
      name: 'John',
      country: 'England',
      city: 'London',
      address: 'Oxford Street',
      zipCode: '12-345',
    },
  },
  {
    key: 'jacob-row',
    value: {
      name: 'Jacob',
      country: 'Denmark',
      city: 'Copenhagen',
      address: 'Amager',
      zipCode: '98-521',
    },
  },
  {
    key: 'piotr-row',
    value: {
      name: 'Piotr',
      country: 'Poland',
      city: 'Zielona Gora',
      address: 'Wojska Polskiego',
      zipCode: '45-123',
    },
  },
];

export const textGrid = [
  {
    title: 'Name',
    description: 'Anna',
    keyValue: 'name-key',
  },
  {
    title: 'Country',
    description: 'France',
    keyValue: 'country-france',
  },
  {
    title: 'City',
    description: 'Name',
    keyValue: 'city-key',
  },
  {
    title: 'Address',
    description: 'Name',
    keyValue: 'address-key',
  },
  {
    title: 'Zip Code',
    description: '123-456',
    keyValue: 'zip-code-key',
  },
];

export const article = {
  title: 'Welcome to the team Piotr!',
  author: 'Christian Wermuth',
  subject: 'New frontend developer in Integration X',
  length: 125,
  createDate: '31-05-2021',
  releaseDate: '31-05-2021',
  text: `Congratulation Piotr, you have written great demo application! We are very happy
    to hire you and doubled salary you requested for. Welcome!`,
};

export const formContent = [
  {
    name: 'title',
    label: 'Title',
    validationSchema: {
      minLength: 2,
      maxLength: 256,
    },
  },
  {
    name: 'author',
    label: 'Author',
    validationSchema: {
      minLength: 2,
      maxLength: 256,
    },
  },
  {
    name: 'subject',
    label: 'Subject',
    validationSchema: {
      minLength: 2,
      maxLength: 256,
    },
  },
  {
    name: 'createDate',
    label: 'Create Date',
    date: true,
  },
  {
    name: 'releaseDate',
    label: 'Release Date',
    date: true,
  },
  {
    name: 'text',
    label: 'Text',
    multiline: true,
    span: true,
    validationSchema: {
      minLength: 2,
      maxLength: 2000,
    },
  },
];

export const formInitialValues = {
  title: '',
  author: 'author',
  subject: 'subject',
  createDate: '2021-05-30',
  releaseDate: '2021-05-30',
  text: 'text',
};

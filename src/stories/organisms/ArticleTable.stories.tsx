import { Story, Meta } from '@storybook/react';
import ArticlesTable, {
  ArticlesTableProps,
} from '../../components/organisms/ArticlesTable';
import { article } from '../../utils/mockedData';

export default {
  title: 'Organism/ArticlesTable',
  component: ArticlesTable,
} as Meta;

const Template: Story<ArticlesTableProps> = (args) => (
  <ArticlesTable {...args} />
);

export const Default = Template.bind({});
Default.args = {
  articles: [{ key: 1, value: article }],
  isFetching: false,
  helperText: undefined,
};

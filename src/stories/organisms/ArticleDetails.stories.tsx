import { Story, Meta } from '@storybook/react';
import ArticleDetails, {
  ArticleDetailsProps,
} from '../../components/organisms/ArticleDetails';
import { article } from '../../utils/mockedData';

export default {
  title: 'Organism/ArticleDetails',
  component: ArticleDetails,
} as Meta;

const Template: Story<ArticleDetailsProps> = (args) => (
  <ArticleDetails {...args} />
);

export const Default = Template.bind({});
Default.args = {
  article: article,
};

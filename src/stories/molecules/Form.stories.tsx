import { Story, Meta } from '@storybook/react';
import Form, { FormProps } from '../../components/molecules/Form';
import { formContent, formInitialValues } from '../../utils/mockedData';

export default {
  title: 'Molecule/Form',
  component: Form,
} as Meta;

const Template: Story<FormProps> = (args) => <Form {...args} />;

export const Default = Template.bind({});
Default.args = {
  content: formContent,
  initialValues: formInitialValues,
};

import { Story, Meta } from '@storybook/react';
import NavBar from '../../components/molecules/NavBar';

export default {
  title: 'Molecule/NavBar',
  component: NavBar,
} as Meta;

const Template: Story = (args) => <NavBar {...args} />;

export const Default = Template.bind({});
Default.args = {};

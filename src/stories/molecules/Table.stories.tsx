import { Story, Meta } from '@storybook/react';
import { tableColumns, tableContent } from '../../utils/mockedData';

import Table, { TableProps } from '../../components/molecules/Table';

export default {
  title: 'Molecule/Table',
  component: Table,
  argTypes: {
    onOpenClick: { action: 'clicked' },
    onDeleteClick: { action: 'clicked' },
  },
} as Meta;

const Template: Story<TableProps> = (args) => <Table {...args} />;

export const TableWithContent = Template.bind({});
TableWithContent.args = {
  action: true,
  columns: tableColumns,
  content: tableContent,
};

export const EmptyTable = Template.bind({});
EmptyTable.args = {
  action: false,
  columns: tableColumns,
  content: [],
  helperText: 'Table is Empty',
};

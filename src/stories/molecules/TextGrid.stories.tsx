import { Story, Meta } from '@storybook/react';
import TextGrid, { TextGridProps } from '../../components/molecules/TextGrid';
import { textGrid } from '../../utils/mockedData';

export default {
  title: 'Molecule/TextGrid',
  component: TextGrid,
} as Meta;

const Template: Story<TextGridProps> = (args) => <TextGrid {...args} />;

export const Default = Template.bind({});
Default.args = {
  content: textGrid,
};

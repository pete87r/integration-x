import { Story, Meta } from '@storybook/react';

import TextCell, { TextCellProps } from '../../components/molecules/TextCell';

export default {
  title: 'Molecule/TextCell',
  component: TextCell,
} as Meta;

const Template: Story<TextCellProps> = (args) => <TextCell {...args} />;

export const Default = Template.bind({});
Default.args = {
  title: 'City',
  description: 'Barcelona',
};

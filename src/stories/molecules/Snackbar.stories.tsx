import { Story, Meta } from '@storybook/react';

import Snackbar, {
  SnackbarProps,
  AlertTypes,
} from '../../components/molecules/Snackbar';

export default {
  title: 'Molecule/Snackbar',
  component: Snackbar,
} as Meta;

const Template: Story<SnackbarProps> = (args) => <Snackbar {...args} />;

export const Success = Template.bind({});
Success.args = {
  type: AlertTypes.SUCCESS,
  open: true,
  message: 'Congratulations! You got a job!',
};

export const Info = Template.bind({});
Info.args = {
  type: AlertTypes.INFO,
  open: true,
  message: 'We are reviewing your app...',
};

export const Error = Template.bind({});
Error.args = {
  type: AlertTypes.ERROR,
  open: true,
  message: 'Sorry but your app sucks!',
};

import { Story, Meta } from '@storybook/react';
import Button, { ButtonProps } from '../../components/atoms/Button';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';

export default {
  title: 'Atom/Button',
  component: Button,
  argTypes: {
    onClick: { action: 'clicked' },
  },
} as Meta;

const Template: Story<ButtonProps> = (args) => <Button {...args} />;

export const ContainedButton = Template.bind({});
ContainedButton.args = {
  label: 'Click Me!',
  disabled: false,
  loading: false,
};

export const IconButton = Template.bind({});
IconButton.args = {
  label: 'Open',
  disabled: false,
  loading: false,
  icon: <ArrowForwardIosIcon />,
};

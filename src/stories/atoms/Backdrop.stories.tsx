import { Story, Meta } from '@storybook/react';

import Backdrop, { BackdropProps } from '../../components/atoms/Backdrop';

export default {
  title: 'Atom/Backdrop',
  component: Backdrop,
} as Meta;

const Template: Story<BackdropProps> = (args) => (
  <Backdrop {...args}>Some dummy text...</Backdrop>
);

export const Default = Template.bind({});
Default.args = {};

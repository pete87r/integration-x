import { Story, Meta } from '@storybook/react';

import TextField, { TextFieldProps } from '../../components/atoms/TextField';

export default {
  title: 'Atom/TextField',
  component: TextField,
  argTypes: {
    onChange: { action: 'onChange' },
  },
} as Meta;

const Template: Story<TextFieldProps> = (args) => <TextField {...args} />;

export const Default = Template.bind({});
Default.args = {
  value: 'input value',
  helperText: '',
  error: false,
  disabled: false,
};

# Integration-X Demo App

### Authors:

- **Piotr Fleszar**
- **Myszka - The Programning Cat**

### Description:

Simple application to menage newspaper articeles - dream of evrey **Editor in Chief**!
Allows you to save / edit / delete / view articles. Timeless design and high responsiveness make you love that product.

### Used Technoligies and Patterns:

- React ;)
- Redux
- Hooks
- Axios
- Material-Ui
- Type Script
- CSS
- Storybooks
- ESLint/Prettier
- Atomic Design

My goal was to create generic and reusable components, thanks that application is easy scalable.
Application is supported by Firebase API and Database.

### What hasn't been done because of time limitations:

- Authentication
- Tests (I added few just as an example)
- Mocked API (Needed for some Storybooks and Tests)

### Access

Application is deployed [here](https://integration-x-demo.web.app/articles)
To run application locally:

- npm install / yarn
- npm start
- application should run on port 3000

Storybook is deployed [here](https://integration-x-demo-storybook.web.app/?path=/story/atom-backdrop--default)
To run storybook locally:

- npm install / yarn (only if you haven't done that running app)
- npm run storybook
- storybook should run on port 6006

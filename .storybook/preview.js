import '../src/index.css'
import { ThemeProvider } from '@material-ui/core/styles';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import store from '../src/store/store';
import theme from '../src/theme/index';
import MockAxios from '../src/utils/MockAxios'

export const decorators = [
  Story => {
    return (
      <ThemeProvider theme={theme}>
        <BrowserRouter>
          <Provider store={store}>
            <MockAxios>
              <Story />
            </MockAxios>
          </Provider>  
        </BrowserRouter>
      </ThemeProvider>
    )
  }
]

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  controls: { expanded: true },
}
